package com.example.cachewebview

import android.app.Application
import android.os.Build
import com.blankj.utilcode.util.ProcessUtils
import com.fan.cachewebview.utils.PreloadEngineUtil

/**
 * @description
 * @date: 2022/3/9 11:34
 * @author: fan
 */
class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()

        // 初始化加载
        if (ProcessUtils.isMainProcess()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PreloadEngineUtil.preloadWebView(this)
            }
        }


    }

}