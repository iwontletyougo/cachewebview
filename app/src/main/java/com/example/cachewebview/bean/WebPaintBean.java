package com.example.cachewebview.bean;

/**
 * @description $
 * @date: 2022/5/9 17:50
 * @author: fan
 */
public class WebPaintBean {

    public String name;
    public String entryType;
    public double startTime;
    public double duration;
}
