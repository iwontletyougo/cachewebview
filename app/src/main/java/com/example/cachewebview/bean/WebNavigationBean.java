package com.example.cachewebview.bean;

/**
 * @description 精简版：仅此有用
 *  完整版举例
 * {
 *         "name":"http://www.baidu.com/#/",
 *         "entryType":"navigation",
 *         "startTime":0,
 *         "duration":711.7000000029802,
 *         "initiatorType":"navigation",
 *         "nextHopProtocol":"",
 *         "workerStart":0,
 *         "redirectStart":0,
 *         "redirectEnd":0,
 *         "fetchStart":17,
 *         "domainLookupStart":17,
 *         "domainLookupEnd":17,
 *         "connectStart":17,
 *         "connectEnd":17,
 *         "secureConnectionStart":0,
 *         "requestStart":17,
 *         "responseStart":17,
 *         "responseEnd":64.90000000223517,
 *         "transferSize":300,
 *         "encodedBodySize":0,
 *         "decodedBodySize":0,
 *         "serverTiming":[
 *
 *         ],
 *         "workerTiming":[
 *
 *         ],
 *         "unloadEventStart":0,
 *         "unloadEventEnd":0,
 *         "domInteractive":593.2000000029802,
 *         "domContentLoadedEventStart":593.3000000007451,
 *         "domContentLoadedEventEnd":593.3000000007451,
 *         "domComplete":711.4000000022352,
 *         "loadEventStart":711.4000000022352,
 *         "loadEventEnd":711.7000000029802,
 *         "type":"navigate",
 *         "redirectCount":0
 *     }
 *
 * @date: 2022/4/19 16:02
 * @author: fan
 */
public class WebNavigationBean {
    public double duration;
}
