package com.example.cachewebview.bean;


public class WebPerfBean {
    // first-paint 页面加载开始到绘制到屏幕的时间
    public long fp;
    // first-contentful-paint 页面加载开始到页面内容在屏幕上完成渲染的时间
    public long fcp;
    // duration 页面加载总时长
    public long du;


    @Override
    public String toString() {
        return "{" +
                "fp（白屏时间）=" + fp +
                ", fcp（渲染时间）=" + fcp +
                ", du（总时长）=" + du +
                '}';
    }
}
