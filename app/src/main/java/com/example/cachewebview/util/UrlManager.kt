package com.example.cachewebview.util

/**
 * @description $
 * @date: 2022/5/9 17:16
 * @author: fan
 */
object UrlManager {
    const val webUrl = "https://m.58.com/sh/?reform=pcfront"
    const val webContent = "web58"

    const val JS_MONITOR =
        "javascript:web.setH5Performance(JSON.stringify(window.performance.timing),  window.location.href , " +
                "JSON.stringify(window.performance.getEntriesByType('paint'))," +
                "JSON.stringify(window.performance.getEntriesByType('navigation')))"
}