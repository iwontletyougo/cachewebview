package com.example.cachewebview

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.webkit.WebSettings
import android.webkit.WebView

/**
 * @description $
 * @date: 2022/5/9 16:03
 * @author: fan
 */
object WebSettingUtil {

    fun webSetting(webView: WebView?) {
        if (webView != null) {
            val webSettings = webView.settings
            // 允许Js使用
            webSettings.javaScriptEnabled = true
            webSettings.javaScriptCanOpenWindowsAutomatically = true
            // 设置可以支持缩放
            webSettings.setSupportZoom(true)
            // 允许LocalStorage
            webSettings.domStorageEnabled = true
            webSettings.builtInZoomControls = true
            // 自适应屏幕
            webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
            // 扩大比例的缩放
            webSettings.useWideViewPort = true
            webSettings.loadWithOverviewMode = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
                }
            }
        }
    }

    fun isNetworkConnected(context: Context): Boolean {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = manager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}