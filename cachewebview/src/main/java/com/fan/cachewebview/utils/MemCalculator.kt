package com.fan.cachewebview.utils

/**
 * @description 内存计算转换工具
 * @date: 2022/2/22 15:11
 * @author: fan
 */
object MemCalculator {

    private val MB = 1024 * 1024
    private val MB_15 = 15 * MB
    private val MB_10 = 10 * MB
    private val MB_5 = 5 * MB

    fun getSize(): Int {
        val maxMemorySize = Runtime.getRuntime().maxMemory()
        val maxSizeByMB = (maxMemorySize / MB).toInt()

        if (maxSizeByMB >= 512) {
            return MB_15
        }

        if (maxSizeByMB >= 256) {
            return MB_10
        }

        return if (maxSizeByMB > 128) {
            MB_5
        } else 0
    }

}