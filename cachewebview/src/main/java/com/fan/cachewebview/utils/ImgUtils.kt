package com.fan.cachewebview.utils

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Environment
import android.view.View
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * @description 保存图片
 * @date: 2021/2/9 15:01
 * @author: fan
 */
object ImgUtils {

    fun getViewBitmap(view: View): Bitmap {
        // 创建对应大小的bitmap
        val bitmap = Bitmap.createBitmap(
            view.width, view.height,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }


    fun checkPictureIsSameColor(bitmap: Bitmap): Boolean {
        val random = Random()
        val bitmapHeight = bitmap.height
        val bitmapWidth = bitmap.width
        val colorOne = bitmap.getPixel(random.nextInt(bitmapWidth), random.nextInt(bitmapHeight))
        val colorTwo =
            bitmap.getPixel(random.nextInt(bitmapWidth / 2), random.nextInt(bitmapHeight / 2))
        val colorThree =
            bitmap.getPixel(random.nextInt(bitmapWidth / 3), random.nextInt(bitmapHeight / 3))
        return colorOne == colorTwo && colorTwo == colorThree && colorOne == colorThree
    }


    fun getImageFromCache(context: Context, urlContent: String): String {
        return context.cacheDir.absolutePath + File.separator.toString() + context.packageName + File.separator.toString() + urlContent + ".jpg"
    }

    fun saveImageToCache(context: Context, bmp: Bitmap, urlContent: String): Boolean {
        // 首先保存图片
        val storePath: String =
            context.cacheDir.absolutePath + File.separator.toString() + context.packageName
        val appDir = File(storePath)
        if (!appDir.exists()) {
            appDir.mkdir()
        }
        val fileName = "$urlContent.jpg"
        val file = File(appDir, fileName)
        try {
            val fos = FileOutputStream(file)
            //通过io流的方式来压缩保存图片
            val isSuccess = bmp.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.flush()
            fos.close()

            return isSuccess
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }


    //保存文件到指定路径
    @Suppress("DEPRECATION")
    fun saveImageToGallery(context: Context, bmp: Bitmap): Boolean {
        // 首先保存图片
        val storePath: String =
            Environment.getExternalStorageDirectory().absolutePath + File.separator.toString() + context.packageName
        val appDir = File(storePath)
        if (!appDir.exists()) {
            appDir.mkdir()
        }
        val fileName = System.currentTimeMillis().toString() + ".jpg"
        val file = File(appDir, fileName)
        try {
            val fos = FileOutputStream(file)
            //通过io流的方式来压缩保存图片
            val isSuccess = bmp.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.flush()
            fos.close()
            try {
                //保存图片后发送广播通知更新数据库
                val uri: Uri = Uri.fromFile(file)
                context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri))
            } catch (ee: Exception) {
                ee.printStackTrace();
            }

            return isSuccess
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }
}