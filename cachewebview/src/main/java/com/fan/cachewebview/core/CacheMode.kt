package com.fan.cachewebview.core

/**
 * @date: 2022/2/22 17:47
 * @author: fan
 */
enum class CacheMode {
    NO_CACHE, CACHE
}