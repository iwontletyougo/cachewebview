package com.fan.cachewebview.core

import android.webkit.WebResourceResponse
import com.fan.cachewebview.bean.CacheRequest
import com.fan.cachewebview.core.interceptor.ResourceInterceptor

/**
 * @description 本地缓存接口
 * @date: 2022/2/23 15:54
 * @author: fan
 */
interface LocalCacheApi {

    /**
     * 获取 WebView资源响应
     */
    fun get(request: CacheRequest): WebResourceResponse?

    /**
     * 新增 缓存用拦截器
     */
    fun addResourceInterceptor(interceptor: ResourceInterceptor)

    /**
     * 回收相关资源
     */
    fun destroy()
}