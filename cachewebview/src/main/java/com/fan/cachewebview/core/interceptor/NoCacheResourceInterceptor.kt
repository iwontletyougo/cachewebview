package com.fan.cachewebview.core.interceptor

import android.content.Context
import com.fan.cachewebview.bean.WebResource
import com.fan.cachewebview.net.OkHttpResourceLoader
import com.fan.cachewebview.net.ResourceLoadApi
import com.fan.cachewebview.net.SourceRequest

/**
 * @description NoCache时 Interceptor
 * @date: 2022/3/1 13:59
 * @author: fan
 */
class NoCacheResourceInterceptor : ResourceInterceptor {

    var mResourceLoader: ResourceLoadApi? = null

    constructor(context: Context) {
        mResourceLoader = OkHttpResourceLoader(context)
    }

    override fun load(chain: Chain): WebResource? {
        var request = chain.getRequest()
        var sourceRequest = SourceRequest(request!!, true)
        var resource = mResourceLoader?.getResource(sourceRequest)
        if (resource != null) {
            return resource
        }
        return chain.process(request)
    }

}