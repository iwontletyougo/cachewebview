package com.fan.cachewebview.core

import android.webkit.WebResourceResponse
import com.fan.cachewebview.bean.WebResource

/**
 * @description 根据WebResource 重组 WebResourceResponse
 * @date: 2022/2/25 16:07
 * @author: fan
 */
interface WebResourceGenerateApi {

    fun generate(resource: WebResource?, urlMime: String?): WebResourceResponse?

}