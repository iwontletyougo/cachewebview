package com.fan.cachewebview.core

import com.fan.cachewebview.core.interceptor.ResourceInterceptor

/**
 * @description 通用开放API
 * @date: 2022/2/17 10:59
 * @author: fan
 */
interface CacheOpenApi {

    fun setCacheMode(mode: CacheMode, config: CacheConfig?)

    fun addResourceInterceptor(interceptor: ResourceInterceptor)

}