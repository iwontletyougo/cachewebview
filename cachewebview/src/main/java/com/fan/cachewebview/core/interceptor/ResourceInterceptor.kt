package com.fan.cachewebview.core.interceptor

import com.fan.cachewebview.bean.WebResource

/**
 * @description 资源拦截器
 * @date: 2022/2/17 14:01
 * @author: fan
 */
interface ResourceInterceptor {

    /**
     * 逐层加载返回的Web资源
     */
    fun load(chain: Chain): WebResource?

}