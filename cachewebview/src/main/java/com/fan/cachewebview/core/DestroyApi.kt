package com.fan.cachewebview.core

/**
 * @description 删除api
 * @date: 2022/2/22 15:23
 * @author: fan
 */
interface DestroyApi {
    fun destroy()
}