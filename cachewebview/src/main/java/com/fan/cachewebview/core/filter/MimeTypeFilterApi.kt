package com.fan.cachewebview.core.filter

/**
 * @description webview 返回资源类型过滤
 * @date: 2022/2/23 14:50
 * @author: fan
 */
interface MimeTypeFilterApi {
    // 是否拦截该资源
    fun isFilter(mimeType: String): Boolean

    // 添加资源类型
    fun addMimeType(mimeType: String)

    // 移除资源类型
    fun removeMimeType(mimeType: String)

    fun clear()
}