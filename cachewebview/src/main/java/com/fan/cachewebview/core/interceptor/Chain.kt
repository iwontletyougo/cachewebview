package com.fan.cachewebview.core.interceptor

import com.fan.cachewebview.bean.CacheRequest
import com.fan.cachewebview.bean.WebResource

/**
 * @description
 * @date: 2022/2/17 11:02
 * @author: fan
 */
class Chain internal constructor(interceptors: List<ResourceInterceptor>) {

    private val mInterceptors: List<ResourceInterceptor> = interceptors
    private var mIndex = -1
    private var mRequest: CacheRequest? = null


    /**
     * 传入请求体
     */
    fun process(request: CacheRequest?): WebResource? {
        // 拦截器数量作为执行的边界条件
        if (++mIndex >= mInterceptors.size) {
            return null
        }
        mRequest = request
        val interceptor = mInterceptors[mIndex]
        return interceptor.load(this)
    }

    /**
     * 返回 缓存请求内容
     */
    fun getRequest(): CacheRequest? {
        return mRequest
    }

}
