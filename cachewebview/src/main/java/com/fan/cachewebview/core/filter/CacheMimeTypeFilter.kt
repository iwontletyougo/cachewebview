package com.fan.cachewebview.core.filter

import java.util.*

/**
 * @description 默认过滤类型
 * @date: 2022/2/23 14:53
 * @author: fan
 */
open class CacheMimeTypeFilter : MimeTypeFilterApi {

    private val mFilterMimeTypes: MutableSet<String>

    override fun isFilter(mimeType: String): Boolean {
        return !mFilterMimeTypes.contains(mimeType)
    }

    override fun addMimeType(mimeType: String) {
        mFilterMimeTypes.add(mimeType)
    }

    override fun removeMimeType(mimeType: String) {
        mFilterMimeTypes.remove(mimeType)
    }

    override  fun clear() {
        mFilterMimeTypes.clear()
    }

    init {
        mFilterMimeTypes = HashSet()

        // HTML
//        addMimeType("text/html")

        // image
        addMimeType("image/gif")
        addMimeType("image/jpeg")
        addMimeType("image/png")
        addMimeType("image/svg+xml")
        addMimeType("image/bmp")
        addMimeType("image/webp")
        addMimeType("image/tiff")
        addMimeType("image/vnd.microsoft.icon")
        addMimeType("image/x-icon")

        // Js
        addMimeType("application/javascript")
        addMimeType("application/ecmascript")
        addMimeType("application/x-ecmascript")
        addMimeType("application/x-javascript")
        addMimeType("text/ecmascript")
        addMimeType("text/javascript")
        addMimeType("text/javascript1.0")
        addMimeType("text/javascript1.1")
        addMimeType("text/javascript1.2")
        addMimeType("text/javascript1.3")
        addMimeType("text/javascript1.4")
        addMimeType("text/javascript1.5")
        addMimeType("text/jscript")
        addMimeType("text/livescript")
        addMimeType("text/x-ecmascript")
        addMimeType("text/x-javascript")

        // css
        addMimeType("text/css")
        // stream
        addMimeType("application/octet-stream")
        // pdf格式
        addMimeType("application/pdf")


    }
}
