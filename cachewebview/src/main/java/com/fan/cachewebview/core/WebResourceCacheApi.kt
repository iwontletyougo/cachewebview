package com.fan.cachewebview.core

import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse

/**
 * @description 获取 WebResourceResponse，返回给webview加载
 * @date: 2022/2/23 15:25
 * @author: fan
 */
interface WebResourceCacheApi : CacheOpenApi, DestroyApi {


    fun getResource(
        request: WebResourceRequest,
        webViewCacheMode: Int,
        userAgent: String
    ): WebResourceResponse?

}