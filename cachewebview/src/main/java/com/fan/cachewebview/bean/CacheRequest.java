package com.fan.cachewebview.bean;

import com.fan.cachewebview.utils.AppUtils;

import java.util.Locale;
import java.util.Map;

/**
 * @description 缓存发起
 * @date: 2022/2/18 10:06
 * @author: fan
 */
public class CacheRequest {

    // 缓存唯一识别 将请求链接转为MD5
    private String key;

    private String url;

    // 资源类型
    private String mime;

    // 设置是否缓存模式
    private boolean cacheMode;

    private int mWebViewCacheMode;

    private Map<String, String> mHeaders;

    private String mUserAgent;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
        // md5 进入DiskLruCache 缓存时必须为小写字母
        this.key = AppUtils.INSTANCE.getMD5(url).toLowerCase(Locale.ROOT);
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public boolean isCacheMode() {
        return cacheMode;
    }

    public void setCacheMode(boolean cacheMode) {
        this.cacheMode = cacheMode;
    }

    public int getmWebViewCacheMode() {
        return mWebViewCacheMode;
    }

    public void setmWebViewCacheMode(int mWebViewCacheMode) {
        this.mWebViewCacheMode = mWebViewCacheMode;
    }

    public Map<String, String> getmHeaders() {
        return mHeaders;
    }

    public void setmHeaders(Map<String, String> mHeaders) {
        this.mHeaders = mHeaders;
    }

    public String getmUserAgent() {
        return mUserAgent;
    }

    public void setmUserAgent(String mUserAgent) {
        this.mUserAgent = mUserAgent;
    }
}
