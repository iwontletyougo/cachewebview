package com.fan.cachewebview.net

import com.fan.cachewebview.bean.WebResource

/**
 * @description web资源具体加载
 * @date: 2022/3/1 11:05
 * @author: fan
 */
interface ResourceLoadApi {


    fun getResource(request: SourceRequest): WebResource?


}