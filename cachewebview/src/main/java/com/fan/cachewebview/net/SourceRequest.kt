package com.fan.cachewebview.net

import com.fan.cachewebview.bean.CacheRequest

/**
 * @description 资源获取简易封装
 * @date: 2022/3/1 10:07
 * @author: fan
 */
class SourceRequest {

     var url: String? = null
     var cacheable = false
     var headers: Map<String, String>? = null
     var userAgent: String? = null
     var webViewCache = 0

    constructor(request: CacheRequest, cacheable: Boolean) {
        this.cacheable = cacheable
        url = request.url
        headers = request.getmHeaders()
        userAgent = request.getmUserAgent()
        webViewCache = request.getmWebViewCacheMode()
    }


}